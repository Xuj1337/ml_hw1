import pandas as pd
import numpy as np

from scripts.errors import rmse, r2
from scripts.k_fold import k_fold
from scripts.model import sgd, predict
from scripts.utils import normalize_features

import xlrd
from xlutils.copy import copy as xl_copy

train_data = pd.read_csv('../Dataset/Training/Features_Variant_1.csv', header=None)

train_data.drop(columns=[37], inplace=True)
data = train_data.values

y = data[:, -1]
x = data[:, :-1]

test_df = pd.read_csv('../Dataset/Testing/Features_TestSet.csv', header=None)
test_df.drop(columns=[37], inplace=True)

y_test = test_df.values[:, -1]
x_test = test_df.values[:, :-1]

x, x_test = normalize_features(x, x_test)

k = 5
x_train, y_train = k_fold(x, y, k)

rmse_list = []
r2_list = []
rmse_test_list = []
r2_test_list = []

weights_list = []
errors_list = []

for i in range(k):
    w, errors = sgd(x_train[i], y_train[i])
    errors_list.append(errors)
    weights_list.append(w)

    y_predicted_train = predict(x_train[i], w)
    rmse_list.append(rmse(y_train[i], y_predicted_train))
    r2_list.append(r2(y_train[i], y_predicted_train))

    y_predicted_test = predict(x_test, w)
    rmse_test_list.append(rmse(y_test, y_predicted_test))
    r2_test_list.append(r2(y_test, y_predicted_test))

sheet_name = "res"
rb = xlrd.open_workbook('../results/result.xls', formatting_info=True)
wb = xl_copy(rb)
ws = wb.get_sheet(sheet_name)

r2_test_list_mean = np.array(r2_test_list).mean()
r2_test_list_std = np.array(r2_test_list).std()

rmse_test_list_mean = np.array(rmse_test_list).mean()
rmse_test_list_std = np.array(rmse_test_list).std()

r2_list_mean = np.array(r2_list).mean()
r2_list_std = np.array(r2_list).std()

rmse_list_mean = np.array(rmse_list).mean()
rmse_list_std = np.array(rmse_list).std()

for i in range(len(r2_test_list)):
    ws.write(1, i + 1, r2_test_list[i])
ws.write(1, 6, r2_test_list_mean)
ws.write(1, 7, r2_test_list_std)

for i in range(len(rmse_test_list)):
    ws.write(2, i + 1, rmse_test_list[i])
ws.write(2, 6, rmse_test_list_mean)
ws.write(2, 7, rmse_test_list_std)

for i in range(len(r2_list)):
    ws.write(3, i + 1, r2_list[i])
ws.write(3, 6, r2_list_mean)
ws.write(3, 7, r2_list_std)

for i in range(len(rmse_list)):
    ws.write(4, i + 1, rmse_list[i])
ws.write(4, 6, rmse_list_mean)
ws.write(4, 7, rmse_list_std)

feature_names = [
    "Page Popularity", "Page Checkins", "Page talking about", "Page Category",
    "X", "X.1", "X.2", "X.3", "X.4", "X.5", "X.6", "X.7", "X.8", "X.9", "X.10", "X.11", "X.12", "X.13", "X.14", "X.15",
    "X.16", "X.17", "X.18", "X.19", "X.20", "X.21", "X.22", "X.23", "X.24",
    "CC1", "CC2", "CC3", "CC4", "CC5",
    "Base time", "Post length", "Post Share Count", "Post Promotion Status",
    "H Local", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
    "Sunday", "Monday Base Time", "Tuesday Base Time", "Wednesday Base Time", "Thursday Base Time", "Friday Base Time",
    "Saturday Base Time", "Sunday Base Time"
]
weights_list = np.array(weights_list)
for i in range(weights_list.shape[1]):
    ws.write(5 + i, 0, 'f{}'.format(i + 1))
    ws.write(5 + i, 8, feature_names[i])
    for j in range(weights_list.shape[0]):
        ws.write(5 + i, j + 1, weights_list[j][i])

wb.save('../results/result.xls')
