import numpy as np


def normalize_features(x_train, x_test):
    x_mean = x_train.mean(axis=0)
    x_std = x_train.std(axis=0)
    x_train = (x_train - x_mean) / x_std
    x_test = (x_test - x_mean) / x_std

    x_train = np.hstack((x_train, np.ones((x_train.shape[0], 1))))
    x_test = np.hstack((x_test, np.ones((x_test.shape[0], 1))))

    return x_train, x_test
