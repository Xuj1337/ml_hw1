from numpy import array
from sklearn.model_selection import KFold


def k_fold(X, y, folds_count=5):
    kfold = KFold(n_splits=folds_count, shuffle=True)
    kfold.get_n_splits(X)
    X_train = []
    y_train = []
    for train_index, test_index in kfold.split(X):
        X_train.append(X[train_index])
        y_train.append(y[train_index])

    return X_train, y_train
