import numpy as np


def mse(y, y_predicted):
    return np.mean(np.square(y - y_predicted))


def rmse(y, y_predicted):
    y_delta = y - y_predicted
    return np.mean(np.square(y_delta)) ** 0.5


def r2(y, y_predicted):
    y_average = y.mean()
    return 1 - (np.sum(np.square(y - y_predicted) / np.sum(np.square(y - y_average))))
