import numpy as np

from scripts.errors import rmse


def gradient(x, y, w):
    batch_size, feature_count = x.shape
    grad = np.zeros((batch_size, feature_count))
    for i in range(batch_size):
        grad[i, :] = x[i, :] * (w @ x[i] - y[i])
    return grad


def predict(x, w):
    return np.matmul(w, x.T)


def sgd(x, y, l_rate=1e-2, max_epoch=20,
        batch_size=1000):
    w = np.random.randn(x.shape[1])  # init w
    errors = []

    iterations = x.shape[0] // batch_size
    if x.shape[0] % batch_size > 0:
        iterations += 1

    for epoch in range(max_epoch):
        p = np.random.permutation(x.shape[0])
        x, y = x[p], y[p]  # Shuffle
        for i in range(iterations):
            x_batch = x[i * batch_size:(i + 1) * batch_size]
            y_batch = y[i * batch_size:(i + 1) * batch_size]
            batch_gradient = gradient(x_batch, y_batch, w)
            w = w - l_rate * np.mean(batch_gradient, axis=0)

        errors.append(rmse(y, predict(x, w)))

    return w, errors
